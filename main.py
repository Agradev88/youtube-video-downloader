import PySimpleGUI as sg
from pytube import YouTube

def executar_download(link, path):
    video = YouTube(link)
    video.streams.get_highest_resolution().download(output_path=path)

layout = [[sg.Text('Cole aqui o link do vídeo que deseja baixar: '), sg.InputText()], # Primeira linha
        [sg.Text('Caminho de salvamento: '), sg.InputText(), sg.FolderBrowse()], # Segunda linha
        [sg.Button('Baixar'), sg.Button('Cancelar')] # Terceira linha
        ]

janela = sg.Window("VideoDownloader", layout)

while True:
    event, value = janela.read()
    if event == 'Cancelar' or event == sg.WIN_CLOSED:
        break
    elif event == 'Baixar':
        executar_download(values[0], values[1])
        sg.popup_ok("Download concluído com sucesso!")

janela.close()

